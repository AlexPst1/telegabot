﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telegram.Bot;
using System.IO;
using System.Collections.ObjectModel;
using System.Diagnostics;


namespace TK_FuckYouBot
{

   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<TelegrammUser> Users;
        TelegramBotClient bot;
        public MainWindow()
        {
            InitializeComponent();
            Users = new ObservableCollection<TelegrammUser>();
            UserList.ItemsSource = Users;

            string token = "1363573620:AAFlYAejltmvMzaUGf_0Zno5eVqQHSY0dR0";

            bot = new TelegramBotClient(token);

            bot.OnMessage += delegate (object sender, Telegram.Bot.Args.MessageEventArgs e) {
                string msg = $"{DateTime.Now}: {e.Message.Chat.FirstName}{e.Message.Chat.Id} {e.Message.Text}";
                File.AppendAllText("data.log", $"{msg}\n");

                Debug.WriteLine(msg);

                this.Dispatcher.Invoke(() =>
                {
                    var person = new TelegrammUser(e.Message.Chat.FirstName, e.Message.Chat.Id);
                    if (!Users.Contains(person)) Users.Add(person);
                    Users[Users.IndexOf(person)].AddMessage($"{person.Nick}: {e.Message.Text}");
                });

            };

            bot.StartReceiving();

            BtnSend.Click += delegate { SendMsg(); };
            txtBxSendMsg.KeyDown += (s, e) => { if (e.Key == Key.Return) { SendMsg(); } };
        }

        public void SendMsg() {
            var concreteUser = Users[Users.IndexOf(UserList.SelectedItem as TelegrammUser)];
            string question = txtBxSendMsg.Text;
           
            string responseMsg = $"FuCKUBot: {txtBxSendMsg.Text}";
            concreteUser.Messages.Add(responseMsg);

           
            bot.SendTextMessageAsync(concreteUser.Id, txtBxSendMsg.Text);
            string logText = $"{DateTime.Now}: >> {concreteUser.Id} {concreteUser.Id}{concreteUser.Nick}{responseMsg}\n";
            File.AppendAllText("data.log", logText);
            txtBxSendMsg.Text = String.Empty;
                
        }
    }
}
